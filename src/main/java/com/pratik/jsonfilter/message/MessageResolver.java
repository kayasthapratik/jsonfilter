package com.pratik.jsonfilter.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Locale;

/**
 * This class resolves i18n messages in code.
 * Messages are available in /src/main/resources/messages_*.properties
 */
@Component
public class MessageResolver {

    @Autowired
    private MessageSource messageSource;

    private MessageSourceAccessor messageSourceAccessor;

    @PostConstruct
    private void init() {
        messageSourceAccessor = new MessageSourceAccessor(messageSource, Locale.ENGLISH);
    }

    public String resolve(String code) {
        return messageSourceAccessor.getMessage(code);
    }
}
