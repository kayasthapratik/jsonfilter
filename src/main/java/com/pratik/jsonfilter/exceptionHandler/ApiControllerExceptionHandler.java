package com.pratik.jsonfilter.exceptionHandler;

import com.pratik.jsonfilter.controller.ApiController;
import com.pratik.jsonfilter.dto.response.ApiError;
import com.pratik.jsonfilter.message.MessageResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * This class handles exceptions that may occur from ApiController like invalid JSON input
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(assignableTypes = ApiController.class)
public class ApiControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageResolver messageResolver;

    /**
     * Handles any general exception
     * */
    @ExceptionHandler({ Throwable.class })
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {

        // For logging purpose
        ex.printStackTrace();

        return new ResponseEntity<>(new ApiError(messageResolver.resolve("api.generalException")),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Handles invalid JSON
     * @return ApiError object with error message and 400 status code
     * */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {

        return new ResponseEntity<>(new ApiError(messageResolver.resolve("api.invalidjson")),
                HttpStatus.BAD_REQUEST);
    }
}
