package com.pratik.jsonfilter.dto.request;

import com.pratik.jsonfilter.dto.response.ResponseProgram;

import java.util.List;
import java.util.Optional;

public class RequestProgram {

    private String country;
    private String description;
    private boolean drm;
    private int episodeCount;
    private String genre;

    private ProgramImage image;

    private String language;
    private ProgramEpisode nextEpisode;
    private String primaryColour;

    private List<ProgramSeason> seasons;

    private String slug;
    private String title;
    private String tvChannel;

    public ResponseProgram toResponseProgram() {
        ResponseProgram responseProgram = new ResponseProgram();

        Optional.ofNullable(this.getImage()).ifPresent(img-> responseProgram.setImage(img.getShowImage()));
        responseProgram.setSlug(this.getSlug());
        responseProgram.setTitle(this.getTitle());

        return responseProgram;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDrm() {
        return drm;
    }

    public void setDrm(boolean drm) {
        this.drm = drm;
    }

    public int getEpisodeCount() {
        return episodeCount;
    }

    public void setEpisodeCount(int episodeCount) {
        this.episodeCount = episodeCount;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public ProgramImage getImage() {
        return image;
    }

    public void setImage(ProgramImage image) {
        this.image = image;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public ProgramEpisode getNextEpisode() {
        return nextEpisode;
    }

    public void setNextEpisode(ProgramEpisode nextEpisode) {
        this.nextEpisode = nextEpisode;
    }

    public String getPrimaryColour() {
        return primaryColour;
    }

    public void setPrimaryColour(String primaryColour) {
        this.primaryColour = primaryColour;
    }

    public List<ProgramSeason> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<ProgramSeason> seasons) {
        this.seasons = seasons;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTvChannel() {
        return tvChannel;
    }

    public void setTvChannel(String tvChannel) {
        this.tvChannel = tvChannel;
    }
}
