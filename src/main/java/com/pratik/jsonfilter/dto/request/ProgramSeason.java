package com.pratik.jsonfilter.dto.request;

public class ProgramSeason {

    private String slug;

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
