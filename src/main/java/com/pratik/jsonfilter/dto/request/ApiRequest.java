package com.pratik.jsonfilter.dto.request;

import java.util.List;

/**
 * DTO for API request
 * */
public class ApiRequest {

    private List<RequestProgram> payload;
    private int skip;
    private int take;
    private int totalRecords;

    public List<RequestProgram> getPayload() {
        return payload;
    }

    public void setPayload(List<RequestProgram> payload) {
        this.payload = payload;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public int getTake() {
        return take;
    }

    public void setTake(int take) {
        this.take = take;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }
}
