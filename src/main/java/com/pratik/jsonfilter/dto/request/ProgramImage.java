package com.pratik.jsonfilter.dto.request;

public class ProgramImage {

    String showImage;

    public String getShowImage() {
        return showImage;
    }

    public void setShowImage(String showImage) {
        this.showImage = showImage;
    }
}
