package com.pratik.jsonfilter.dto.response;

import java.util.List;

/**
 * DTO for API response
 * */
public class ApiResponse {

    private List<ResponseProgram> response;

    public ApiResponse(List<ResponseProgram> responsePrograms) {
        this.response = responsePrograms;
    }

    public List<ResponseProgram> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseProgram> response) {
        this.response = response;
    }
}
