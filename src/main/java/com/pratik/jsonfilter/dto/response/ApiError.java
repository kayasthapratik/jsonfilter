package com.pratik.jsonfilter.dto.response;

/**
 * DTO for API response in case of any error
 * */
public class ApiError {

    String error;

    public ApiError(String errorMessage) {
        this.error = errorMessage;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
