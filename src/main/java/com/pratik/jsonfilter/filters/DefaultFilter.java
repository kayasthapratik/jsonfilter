package com.pratik.jsonfilter.filters;

import com.pratik.jsonfilter.dto.request.RequestProgram;
import com.pratik.jsonfilter.dto.response.ResponseProgram;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Default filter; drm: true & episodeCount>0
 */
@Component
@Qualifier("default")
public class DefaultFilter implements IFilter {

    @Override
    public List<ResponseProgram> filter(List<RequestProgram> requestPrograms) {
        if (requestPrograms==null) {
            return new ArrayList<ResponseProgram>();
        }

        return requestPrograms.stream()
                .filter(program -> program.isDrm() && program.getEpisodeCount()>0)
                .map(RequestProgram::toResponseProgram)
                .collect(Collectors.toList());
    }
}
