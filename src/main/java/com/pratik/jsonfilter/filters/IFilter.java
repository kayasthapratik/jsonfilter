package com.pratik.jsonfilter.filters;

import com.pratik.jsonfilter.dto.request.RequestProgram;
import com.pratik.jsonfilter.dto.response.ResponseProgram;

import java.util.List;

/**
 * Interface for filters
 * This will be useful if we want to implement more than one type of filters
 * Currently, we just have 1 filter: drm: true & episodeCount>0
 */
public interface IFilter {

    /**
     * Filters the requests according to certain rule.
     *
     * @param requestPrograms List of request program parsed from JSON request.
     * @return List of response-program with only three properties
     */
    List<ResponseProgram> filter(List<RequestProgram> requestPrograms);

}
