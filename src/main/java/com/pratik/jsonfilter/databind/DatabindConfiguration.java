package com.pratik.jsonfilter.databind;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

/**
 * Registering custom objectmapper with custom modules
 * StringSantizeModule in this case.
 * */
@Component
public class DatabindConfiguration {

    @Bean
    protected MappingJackson2HttpMessageConverter registerCustomStringSanitizationModule() {
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new StringSanitizeModule());
        jsonConverter.setObjectMapper(objectMapper);

        return jsonConverter;
    }
}
