package com.pratik.jsonfilter.controller;

import com.pratik.jsonfilter.dto.request.ApiRequest;
import com.pratik.jsonfilter.dto.response.ApiResponse;
import com.pratik.jsonfilter.dto.response.ResponseProgram;
import com.pratik.jsonfilter.message.MessageResolver;
import com.pratik.jsonfilter.service.FilteringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class ApiController {

    @Autowired
    private MessageResolver messageResolver;

    @Autowired
    private FilteringService filteringService;

    /**
     * Handles GET request to the end-point
     * @return invalidMethodMessage, actual value in application.properties
     * */
    @GetMapping
    public String handleGet() {
        return messageResolver.resolve("api.invalidMethod");
    }

    /**
     * Handles POST requests, entry-point for the API
     * @param apiRequest ApiRequest JSON from request is parsed into this object automatically
     * @return apiResponse ApiResponse object with filtered Programs, is converted into JSON automatically
     * */
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse> filterJson(@RequestBody ApiRequest apiRequest) {

        List<ResponseProgram> resultPrograms = filteringService.filterRequest(apiRequest.getPayload());

        return new ResponseEntity<ApiResponse>(new ApiResponse(resultPrograms), HttpStatus.OK);
    }
}
