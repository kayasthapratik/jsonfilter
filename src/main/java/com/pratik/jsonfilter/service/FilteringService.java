package com.pratik.jsonfilter.service;

import com.pratik.jsonfilter.dto.request.RequestProgram;
import com.pratik.jsonfilter.dto.response.ResponseProgram;
import com.pratik.jsonfilter.filters.IFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilteringService {

    @Autowired
    @Qualifier("default")
    private IFilter defaultFilter;

    public List<ResponseProgram> filterRequest(List<RequestProgram> requestPrograms) {
        return defaultFilter.filter(requestPrograms);
    }
}
