package com.pratik.jsonfilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonFilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsonFilterApplication.class, args);
	}
}
