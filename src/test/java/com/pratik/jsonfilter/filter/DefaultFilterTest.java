package com.pratik.jsonfilter.filter;

import com.pratik.jsonfilter.dto.request.RequestProgram;
import com.pratik.jsonfilter.dto.response.ResponseProgram;
import com.pratik.jsonfilter.filters.DefaultFilter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultFilterTest {

    @InjectMocks
    private DefaultFilter defaultFilter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDefaultFilter() {
        List<RequestProgram> requestProgramList = new ArrayList<>();

        RequestProgram one = new RequestProgram();
        one.setDrm(true);
        one.setEpisodeCount(3);
        one.setTitle("title-one");
        requestProgramList.add(one);

        RequestProgram two = new RequestProgram();
        two.setDrm(true);
        two.setEpisodeCount(45);
        two.setTitle("title-two");
        requestProgramList.add(two);

        RequestProgram three = new RequestProgram();
        three.setDrm(false);
        three.setEpisodeCount(3);
        three.setTitle("title-three");
        requestProgramList.add(three);

        RequestProgram four = new RequestProgram();
        four.setDrm(true);
        requestProgramList.add(four);

        List<ResponseProgram> result = defaultFilter.filter(requestProgramList);

        assertThat(result.size()).isEqualTo(2);

        assertThat(result.stream().filter(program -> program.getTitle().equals("title-one")).count()).isEqualTo(1);
        assertThat(result.stream().filter(program -> program.getTitle().equals("title-two")).count()).isEqualTo(1);
        assertThat(result.stream().filter(program -> program.getTitle().equals("title-three")).count()).isEqualTo(0);
        assertThat(result.stream().filter(program -> program.getTitle().equals("title-four")).count()).isEqualTo(0);
    }

    @Test
    public void testWhenResultIsEmpty() {
        List<RequestProgram> requestProgramList = new ArrayList<>();

        RequestProgram one = new RequestProgram();
        one.setDrm(false);
        one.setEpisodeCount(3);
        one.setTitle("title-one");
        requestProgramList.add(one);

        RequestProgram two = new RequestProgram();
        two.setDrm(true);
        two.setEpisodeCount(0);
        two.setTitle("title-two");
        requestProgramList.add(two);

        RequestProgram three = new RequestProgram();
        three.setDrm(false);
        three.setEpisodeCount(3);
        three.setTitle("title-three");
        requestProgramList.add(three);

        List<ResponseProgram> responsePrograms = defaultFilter.filter(requestProgramList);

        assertThat(responsePrograms.isEmpty()).isTrue();
    }

    @Test
    public void testWithEmptyRequest() {
        List<RequestProgram> requestPrograms = new ArrayList<>();

        List<ResponseProgram> responsePrograms = defaultFilter.filter(requestPrograms);

        assertThat(responsePrograms.isEmpty()).isTrue();
    }

    @Test
    public void testWithNullRequest() {
        List<ResponseProgram> responsePrograms = defaultFilter.filter(null);

        assertThat(responsePrograms.isEmpty()).isTrue();
    }
}
