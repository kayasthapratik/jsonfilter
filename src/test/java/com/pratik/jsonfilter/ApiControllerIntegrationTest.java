package com.pratik.jsonfilter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.StreamUtils;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Integration test for ApiController
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JsonFilterApplication.class)
public class ApiControllerIntegrationTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testNormalApiRequest() throws Exception {
        Resource requestJson = new ClassPathResource("apiRequest.json");
        mockMvc.perform(post("/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(StreamUtils.copyToString(requestJson.getInputStream(), Charset.defaultCharset())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.response.length()").value(2));
    }

    @Test
    public void testWithEmptyRequest() throws Exception {
        mockMvc.perform(post("/")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"payload\": [],\"skip\": 0,\"take\": 10,\"totalRecords\": 75}"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.response").exists());
    }

    @Test
    public void testInvalidJson() throws Exception {
        mockMvc.perform(post("/")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"payload\": [],\"skip\": 0,\"take\": 10,\"to"))
                    .andExpect(status().is(400))
                    .andExpect(jsonPath("$.error").exists());
    }
}
