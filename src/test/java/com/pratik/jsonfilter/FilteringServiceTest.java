package com.pratik.jsonfilter;

import com.pratik.jsonfilter.service.FilteringService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * This simply tests if the filtering service is initialized
 * since there is no any functionality in this service; it simply calls filters
 * */
@SpringBootTest
@RunWith(SpringRunner.class)
public class FilteringServiceTest {

    @Autowired
    private FilteringService filteringService;

    @Test
    public void testFilteringServiceIsAutowired() {
        assertThat(filteringService).isNotNull();
    }

}
