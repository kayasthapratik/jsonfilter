package com.pratik.jsonfilter.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pratik.jsonfilter.dto.response.ApiResponse;
import com.pratik.jsonfilter.dto.response.ResponseProgram;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * This tests if the ApiResponse DTO can is converted to JSON correctly.
 * * We want to use the same objectmapper as in main app, so running this test with spring context
 * */
@RunWith(SpringRunner.class)
@JsonTest
public class ApiResponseJsonTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private JacksonTester<ApiResponse> json;

    @Before
    public void setup() {
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void testJsonConversion() throws IOException {
        List<ResponseProgram> responseProgramList = new ArrayList<>();

        ResponseProgram sixteenAndCountring = new ResponseProgram();
        sixteenAndCountring.setImage("http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg");
        sixteenAndCountring.setSlug("show/16kidsandcounting");
        sixteenAndCountring.setTitle("16 Kids and Counting");
        responseProgramList.add(sixteenAndCountring);

        ResponseProgram theTaste = new ResponseProgram();
        theTaste.setImage("http://mybeautifulcatchupservice.com/img/shows/TheTaste1280.jpg");
        theTaste.setSlug("show/thetaste");
        theTaste.setTitle("The Taste");
        responseProgramList.add(theTaste);

        ResponseProgram thunderbirds = new ResponseProgram();
        thunderbirds.setImage("http://mybeautifulcatchupservice.com/img/shows/Thunderbirds_1280.jpg");
        thunderbirds.setSlug("show/thunderbirds");
        thunderbirds.setTitle("Thunderbirds");
        responseProgramList.add(thunderbirds);

        ApiResponse apiResponse = new ApiResponse(responseProgramList);

        assertThat(this.json.write(apiResponse)).isEqualToJson(new ClassPathResource("apiResponse.json"));
    }

    @Test
    public void testEmptyResponse() throws IOException {
        ApiResponse apiResponse = new ApiResponse(new ArrayList<ResponseProgram>());

        assertThat(this.json.write(apiResponse)).isEqualToJson(new ByteArrayInputStream("{\"response\":[]}".getBytes(StandardCharsets.UTF_8)));
    }
}
