package com.pratik.jsonfilter.dto;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pratik.jsonfilter.dto.request.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * This tests if the ApiRequest DTO can parse JSON correctly
 * We want to use the same objectmapper as in main app, so running this test with spring context
 * */
@RunWith(SpringRunner.class)
@JsonTest
public class ApiRequestJsonTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private JacksonTester<ApiRequest> json;

    @Before
    public void setup() {
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void testJsonParsing() throws Exception {
        ApiRequest apiRequest = buildNormalApiRequest();

        Path path = Paths.get(getClass().getClassLoader().getResource("apiRequest.json").toURI());
        byte[] fileBytes = Files.readAllBytes(path);

        assertThat(this.json.parse(new String(fileBytes))).isEqualToComparingFieldByFieldRecursively(apiRequest);
    }

    @Test(expected = JsonMappingException.class)
    public void testInvalidJson() throws Exception {
        ApiRequest apiRequest = new ApiRequest();

        assertThat(this.json.parse("")).isEqualToComparingFieldByFieldRecursively(apiRequest);
    }

    private ApiRequest buildNormalApiRequest() {
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setSkip(0);
        apiRequest.setTake(10);
        apiRequest.setTotalRecords(75);

        List<RequestProgram> payload = new ArrayList<>();

        RequestProgram sixteenAndCounting = new RequestProgram();
        sixteenAndCounting.setCountry("UK");
        sixteenAndCounting.setDescription("What's life like when you have enough children to field your own football team?");
        sixteenAndCounting.setDrm(true);
        sixteenAndCounting.setEpisodeCount(3);
        sixteenAndCounting.setGenre("Reality");

        ProgramImage sixteenAndCountingImage = new ProgramImage();
        sixteenAndCountingImage.setShowImage("http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg");
        sixteenAndCounting.setImage(sixteenAndCountingImage);

        sixteenAndCounting.setLanguage("English");
        sixteenAndCounting.setPrimaryColour("#ff7800");

        List<ProgramSeason> sixteenAndCountingSeasons = new ArrayList<>();
        ProgramSeason sixteenAndCountingSeasonOne = new ProgramSeason();
        sixteenAndCountingSeasonOne.setSlug("show/16kidsandcounting/season/1");
        sixteenAndCountingSeasons.add(sixteenAndCountingSeasonOne);
        sixteenAndCounting.setSeasons(sixteenAndCountingSeasons);

        sixteenAndCounting.setSlug("show/16kidsandcounting");
        sixteenAndCounting.setTitle("16 Kids and Counting");
        sixteenAndCounting.setTvChannel("GEM");
        payload.add(sixteenAndCounting);

        RequestProgram seaPatrol = new RequestProgram();
        seaPatrol.setSlug("show/seapatrol");
        seaPatrol.setTvChannel("Channel 9");
        seaPatrol.setTitle("Sea Patrol");
        payload.add(seaPatrol);

        RequestProgram theTaste = new RequestProgram();
        theTaste.setCountry("USA");
        theTaste.setDescription("The Taste puts 16 culinary competitors in the kitchen, where four of the World's most notable culinary masters of the food world judges their creations based on a blind taste. Join judges Anthony Bourdain, Nigella Lawson, Ludovic Lefebvre and Brian Malarkey in this pressure-packed contest where a single spoonful can catapult a contender to the top or send them packing.");
        theTaste.setDrm(true);
        theTaste.setEpisodeCount(2);
        theTaste.setGenre("Reality");

        ProgramImage theTasteImage = new ProgramImage();
        theTasteImage.setShowImage("http://mybeautifulcatchupservice.com/img/shows/TheTaste1280.jpg");
        theTaste.setImage(theTasteImage);

        theTaste.setLanguage("English");

        ProgramEpisode theTasteNextEpisode = new ProgramEpisode();
        theTasteNextEpisode.setChannelLogo("http://mybeautifulcatchupservice.com/img/player/logo_go.gif");
        theTasteNextEpisode.setHtml("<br><span class=\"visit\">Visit the Official Website</span></span>");
        theTasteNextEpisode.setUrl("http://go.ninemsn.com.au/");
        theTaste.setNextEpisode(theTasteNextEpisode);

        theTaste.setPrimaryColour("#df0000");

        List<ProgramSeason> theTasteSeasons = new ArrayList<>();
        ProgramSeason theTasteSeasonOne = new ProgramSeason();
        theTasteSeasonOne.setSlug("show/thetaste/season/1");
        theTasteSeasons.add(theTasteSeasonOne);
        theTaste.setSeasons(theTasteSeasons);

        theTaste.setSlug("show/thetaste");
        theTaste.setTitle("The Taste");
        theTaste.setTvChannel("GEM");
        payload.add(theTaste);

        apiRequest.setPayload(payload);

        return apiRequest;
    }
}
