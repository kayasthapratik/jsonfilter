package com.pratik.jsonfilter.dto;

import com.pratik.jsonfilter.dto.request.ProgramImage;
import com.pratik.jsonfilter.dto.request.ProgramSeason;
import com.pratik.jsonfilter.dto.request.RequestProgram;
import com.pratik.jsonfilter.dto.response.ResponseProgram;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

public class RequestProgramTest {

    @Test
    public void testConversionToResponseProgram() {
        RequestProgram sixteenAndCounting = new RequestProgram();
        sixteenAndCounting.setCountry("UK");
        sixteenAndCounting.setDescription("What's life like when you have enough children to field your own football team?");
        sixteenAndCounting.setDrm(true);
        sixteenAndCounting.setEpisodeCount(3);
        sixteenAndCounting.setGenre("Reality");

        ProgramImage sixteenAndCountingImage = new ProgramImage();
        sixteenAndCountingImage.setShowImage("http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg");
        sixteenAndCounting.setImage(sixteenAndCountingImage);

        sixteenAndCounting.setLanguage("English");
        sixteenAndCounting.setPrimaryColour("#ff7800");

        List<ProgramSeason> sixteenAndCountingSeasons = new ArrayList<>();
        ProgramSeason sixteenAndCountingSeasonOne = new ProgramSeason();
        sixteenAndCountingSeasonOne.setSlug("show/16kidsandcounting/season/1");
        sixteenAndCountingSeasons.add(sixteenAndCountingSeasonOne);
        sixteenAndCounting.setSeasons(sixteenAndCountingSeasons);

        sixteenAndCounting.setSlug("show/16kidsandcounting");
        sixteenAndCounting.setTitle("16 Kids and Counting");
        sixteenAndCounting.setTvChannel("GEM");

        ResponseProgram sixteenAndCountingResponse = sixteenAndCounting.toResponseProgram();

        assertThat(sixteenAndCountingResponse.getImage()).isEqualTo(sixteenAndCounting.getImage().getShowImage());
        assertThat(sixteenAndCountingResponse.getSlug()).isEqualTo(sixteenAndCounting.getSlug());
        assertThat(sixteenAndCountingResponse.getTitle()).isEqualTo(sixteenAndCounting.getTitle());
    }

    @Test
    public void testEmptyConversionToResponseProgram() {
        RequestProgram sixteenAndCounting = new RequestProgram();
        ResponseProgram responseProgram = sixteenAndCounting.toResponseProgram();

        assertThat(responseProgram.getTitle()).isNull();
        assertThat(responseProgram.getImage()).isNull();
        assertThat(responseProgram.getSlug()).isNull();
    }
}
