package com.pratik.jsonfilter;

import com.pratik.jsonfilter.controller.ApiController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * This simply tests if the controller is initialized
 * */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ApiControllerTest {

    @Autowired
    private ApiController apiController;

    @Test
    public void controllerInitializedCorrectly() {
        assertThat(apiController).isNotNull();
    }
}
