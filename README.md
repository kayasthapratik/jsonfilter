# JSONFilter

Filters json according to a certain criteria
In this case, only returns if drm is true and episodeCount is greater than 0.

## How to Run

You can run the app by running the following command. You will need JDK1.8 installed.

```
gradlew bootRun
```

The app runs on port 9090.

## The tests

The integration test is under src/main/test/java/com/pratik/jsonfilter along with all other unit tests.

The integration test 'ApiControllerIntegrationTest' test three scenarios: with normal JSON request, with empty payload and with invalid JSON body.
The API is expected to return a valid JSON response in all three cases with suitable response/error codes.